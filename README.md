# Gestão de Configuração de Software para Sistema de execução de customização de dados em Banco.

O sistema se trata de uma biblioteca python para executar pipelines de dados de forma customizável em bancos de dados.

Para executar a aplicação em sua máquina, basta seguir o passo-a-passo descritos abaixo.

# Resumo da aplicação 

 A biblioteca desenvolvida auxilia desenvolvedores a explorar os dados com funções essenciais para a identificação de outliers e anomalias e uma interface que auxilia a visualizar as informações de acordo com o arquivo de configuração.

 A biblioteca recebe um arquivo yaml com as configurações de cada etapa do pipeline de dados, e do endereço do banco de dados.
 Após a execução do banco de dados os dados são atualizados com os resultados da análise e os resultados podem ser visualizados por meio de dashboards no metabase.


### Requisitos de instação

```
python -m venv env
source env/bin/activate
pip install -r requirements.txt
```

### Rodando a aplicação

```
python application/main.py
```

### Testando

```
pytest --cov
```

### Metabase

O metabase ajuda a visualizar e a modelar o processamento dos dados, a engenharia de features e monitoramento do modelo.



| Keywords  | Descrição |
|-----------|-------------|
|   CSV     | Um arquivo CSV é um arquivo de texto simples que armazena informações de tabelas e planilhas. Os arquivos CSV podem ser facilmente importados e exportados usando programas que armazenam dados em tabelas.|
| Collection (coleção)| Uma coleção é um agrupamento de documentos do MongoDB. Os documentos dentro de uma coleção podem ter campos diferentes. Uma coleção é o equivalente a uma tabela em um sistema de banco de dados relacional.|
|  Database | Um banco de dados armazena uma ou mais coleções de documentos.|
| Mongo| É um banco de dados NoSQL desenvolvido pela MongoDB Inc. O banco de dados MongoDB foi criado para armazenar uma grande quantidade de dados e também executar rapidamente.|



**Connect the database to the metabase**

- step 1: Open localhost:3000
- step 2: Click Admin setting
- step 3: Click Database
- step 4: Adicione os dados de autenticação de  banco de dados 


**Exemplo da conexão mongo  metabase**
|  metabase  | credential  |
|------------|-------------|
|    host    |  mongo  |
|dabase_name | use the name you define in make migrate|
|    user    |   lappis    |
|  password  |   lappis    |

